/*-----------------------------------------*\
|  RGBController_DrevoKeyboard.h          |
|                                           |
|  Generic RGB Interface for Drevo RGB    |
|  keyboard devices                         |
|                                           |
|  Diogo Trindade (diogotr7)    3/4/2021    |
\*-----------------------------------------*/

#pragma once
#include "RGBController.h"
#include "DrevoKeyboardController.h"

class RGBController_DrevoKeyboard : public RGBController
{
public:
    RGBController_DrevoKeyboard(DrevoKeyboardController* drevo_ptr);
    ~RGBController_DrevoKeyboard();

    void        SetupZones();

    void        ResizeZone(int zone, int new_size);
    
    void        DeviceUpdateLEDs();
    void        UpdateZoneLEDs(int zone);
    void        UpdateSingleLED(int led);

    void        SetCustomMode();
    void        DeviceUpdateMode();

private:
    DrevoKeyboardController*  drevo;
};
