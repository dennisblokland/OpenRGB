/*-----------------------------------------*\
|  DrevoKeyboardController.cpp            |
|                                           |
|  Driver for Drevo RGB keyboardlighting  |
|  controller                               |
|                                           |
|  Diogo Trindade (diogotr7)    3/4/2021    |
\*-----------------------------------------*/

#include <cstring>
#include "DrevoKeyboardController.h"


//0xFFFFFFFF indicates an unused entry in matrix
#define NA 0xFFFFFFFF

static const unsigned int rgb_led_index[6][17] =
{
    {0x29,    NA,		0x3a,     0x3b,       0x3c,       0x3d,      0x3e,     0x3f,     0x40,    0x41,    0x42,      0x43,      0x44,     0x45,  0x46,    0x47 ,   0x48},
    {0x35,    0x1e,     0x1f,     0x20,       0x21,       0x22,      0x23,     0x24,     0x25,    0x26,    0x27,      0x2d,      0x2e,     0x2a,  0x49,    0x4a,    0x4b},
    {0x2b,    0x14,     0x1a,     0x08,       0x15,       0x17,      0x1c,     0x18,     0x0c,    0x12,    0x13,      0x2f,      0x30,     0x31,  0x4c,    0x4d,    0x4e},
    {0x39,    0x04,     0x16,     0x07,       0x09,       0x0a,      0x0b,     0x0d,     0x0e,    0x0f,    0x33,      0x34, 	 NA,       0x28,  NA,      NA,      NA},
    {0xe1,    NA,		0x1d,     0x1b,       0x06,       0x19,      0x05,     0x11,     0x10,    0x36,    0x37,      0x38,      NA,	   0xe5,  NA,      0x52,    NA},
    {0xe0,    0xe3 ,    0xe2 ,    NA,         NA,         NA,        0x2c,     NA,       NA,      NA,      0xe6,      0xed,      0x65,     0xe4,  0x50,    0x51,    0x4f}
};

DrevoKeyboardController::DrevoKeyboardController(hid_device* dev_handle, const char* path)
{
    dev = dev_handle;
    location    = path;
}

DrevoKeyboardController::~DrevoKeyboardController()
{
    
}

std::string DrevoKeyboardController::GetDeviceLocation()
{
    return("HID: " + location);
}

void DrevoKeyboardController::SendDirect(RGBColor* colors, unsigned int num_colors)
{
   
    int count = 0;
    int count2 = 0;
    unsigned char buf[32] = { 0 };
    buf[0] = 0x06;
    buf[1] = 0xbe;
    buf[2] = 0x19;
    buf[3] = 0x00;
    buf[4] = 0x01;
    buf[5] = 0x01;
    buf[6] = 0x0e;

    for(std::size_t color_idx = 0; color_idx < num_colors; color_idx++)
    {
        unsigned char led_index = rgb_led_index[color_idx % 6][color_idx / 6];
        if (led_index != NA)
        {


            buf[7 + count] = led_index;

            int color_pos = count * 4;

            buf[12 + color_pos] = RGBGetRValue(colors[color_idx]);
            buf[13 + color_pos] = RGBGetGValue(colors[color_idx]);
            buf[14 + color_pos] = RGBGetBValue(colors[color_idx]);


            if (count == 4)
            {
                count = 0;
                hid_write(dev, buf, 32);
            }
            else
                if (count2 == 101)
                {
                    hid_write(dev, buf, 32);
                }
            count++;
            count2++;
        }
    }
}




