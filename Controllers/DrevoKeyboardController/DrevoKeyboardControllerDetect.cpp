#include "Detector.h"
#include "DrevoKeyboardController.h"
#include "RGBController.h"
#include "RGBController_DrevoKeyboard.h"
#include <vector>
#include <hidapi/hidapi.h>

/*-----------------------------------------------------*\
| Drevo vendor ID                                     |
\*-----------------------------------------------------*/
#define DREVO_VID                       0x0416

/*-----------------------------------------------------*\
| Keyboard product IDs                                  |
\*-----------------------------------------------------*/
#define DREVO_TYRFING_PID                   0xA0F8

typedef struct
{
    unsigned short  usb_vid;
    unsigned short  usb_pid;
    const char *    name;
} drevo_device;

#define DREVO_NUM_DEVICES (sizeof(device_list) / sizeof(device_list[0]))

static const drevo_device device_list[] =
{
    /*-----------------------------------------------------------------------*\
    | Keyboards                                                               |
    \*-----------------------------------------------------------------------*/
    { DREVO_VID,              DREVO_TYRFING_PID,              "Tyrfing V2"   },
};

/******************************************************************************************\
*                                                                                          *
*   DetectDrevoKeyboardControllers                                                       *
*                                                                                          *
*       Tests the USB address to see if a Drevo RGB Keyboard controller exists there.    *
*                                                                                          *
\******************************************************************************************/
void DetectDrevoKeyboardControllers(hid_device_info* info, const std::string& name)
{
    if( info->usage == 0){
    hid_device* dev = hid_open_path(info->path);
        if( dev )
        {
            DrevoKeyboardController* controller = new DrevoKeyboardController(dev, info->path);
            RGBController_DrevoKeyboard* rgb_controller = new RGBController_DrevoKeyboard(controller);
            rgb_controller->name = name;
            ResourceManager::get()->RegisterRGBController(rgb_controller);
        }
    }
}
REGISTER_HID_DETECTOR_I("Drevo Tyrfing V2", DetectDrevoKeyboardControllers, DREVO_VID, DREVO_TYRFING_PID, 1);


