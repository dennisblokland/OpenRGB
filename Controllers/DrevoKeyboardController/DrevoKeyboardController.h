/*-----------------------------------------*\
|  DrevoKeyboardController.h              |
|                                           |
|  Definitions and types for Drevo RGB    |
|  keyboard lighting controller             |
|                                           |
|  Diogo Trindade (diogotr7)    3/4/2021    |
\*-----------------------------------------*/

#include "RGBController.h"

#include <string>
#include <hidapi/hidapi.h>

#pragma once


class DrevoKeyboardController
{
public:
    DrevoKeyboardController(hid_device* dev_handle, const char* path);
    ~DrevoKeyboardController();
    std::string GetDeviceLocation();

    void SendDirect(RGBColor* colors, unsigned int num_colors);

private:
    hid_device*             dev;
    std::string             location;

};
